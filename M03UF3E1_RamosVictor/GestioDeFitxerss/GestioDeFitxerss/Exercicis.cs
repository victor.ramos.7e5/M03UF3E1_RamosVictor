﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace GestioDeFitxerss
{
    class Exercicis
    {

        public bool HaveExtension(string path)
        {
            return Path.HasExtension(path);
        }


     
        public bool DirectorioExist(string path)
        {
            return Directory.Exists(path);
        }

    
        public bool ArchivoExist(string path)
        {
            return File.Exists(path);
        }

        public void ShowRutaAbsoluta(string path)
        {
            Console.WriteLine("El path absoluto a ({0}) es: {1}", path, Path.GetFullPath(path));
        }



        public void ListarDirectorioFicheroInfo(string path)
        {
            var carpeta = new DirectoryInfo(path);

            var dires = carpeta.EnumerateDirectories();
            foreach (var directorio in dires)
                Console.WriteLine(directorio.Name + " <DIR>");
            var filtro = carpeta.EnumerateFiles();

            foreach (var fichero in filtro)
                Console.WriteLine(fichero.Name + " <FILE>");
        }
  
        public void LlistarDirectorioFicheroString(string path)
        {
            //Directorios <DIR>
            List<string> dirs = new List<string>(Directory.EnumerateDirectories(path));
            foreach (var directorio in dirs)
            {
                Console.WriteLine(directorio + " <DIR>");
            }
            //Ficheros <FILE>
            IEnumerable<string> txtFiles = Directory.EnumerateFiles(path);

            foreach (string archivo in txtFiles)
            {
                Console.WriteLine(archivo + " <FILE>");
            }
        }

   
        public void BorrarDirectorio(string path)
        {
            try
            {
                Directory.Delete(path);
            }
            catch (IOException)
            {
                string a = EntrarDades.GetUpperString("El directori no está buit ¿Vols continuar? [S] [N]");
                if (a == "S")
                {
                    Directory.Delete(path, true);
                }
            }

        }
   
        public void CrearDirectori(string path)
        {
            try
            {
                Directory.CreateDirectory(path);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();

            }
        }

        public void ChangeName(string path)
        {
            string name = EntrarDades.GetString("Introduce un nuevo nombre.");
            try
            {
                string pathNew = Path.Combine(path + "/..", name);
                Directory.Move(path, pathNew);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();

            }
        }


        public void ListarDirectorioFicheroInfoRecursive(string path)
        {
            var carpeta = new DirectoryInfo(path);
            //Directorios <DIR>
            var dires = carpeta.EnumerateDirectories();
            foreach (var directorio in dires)
            {
                Console.WriteLine(directorio.Name + " <DIR> " + Directory.GetLastWriteTime(directorio.ToString()));
                string pathNew = Path.Combine(path + "/", directorio.Name);
                ListarDirectorioFicheroInfoRecursive(pathNew);
            }
            //Ficheros <FILE>
            var filtro = carpeta.EnumerateFiles();

            foreach (var fichero in filtro)
                Console.WriteLine(fichero.Name + " " + File.GetLastWriteTime(fichero.ToString()) + " " + fichero.Length);
        }
    }
}
}
