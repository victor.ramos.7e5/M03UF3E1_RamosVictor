﻿using System;
using System.IO;

namespace GestioDeFitxerss
{
    class Program
    {
   
        public static void Main()
        {
            Program programa = new Program();
            programa.Inicio();
        }

   
        void Inicio()
        {
            bool fin;
            do
            {
                Console.Clear();
                MostrarMenu();
                string opcion = EntrarDades.GetString("Introdueix una opció");
                fin = CallExercice(opcion);
            } while (!fin);
        }

        void MostrarMenu()
        {
            Console.WriteLine("[1]: Exercici 1");
            Console.WriteLine("[2]: Exercici 2");
            Console.WriteLine("[3]: Exercici 3");
            Console.WriteLine("[4]: Exercici 4");
            Console.WriteLine("[0]: Sortir");
        }


        bool CallExercice(string opcion)
        {
            Console.Clear();
            switch (opcion)
            {
                case "1":
                    Ejercicio_1();
                    break;
                case "2":
                    Ejercicio_2();
                    break;
                case "3":
                    Ejercicio_3();
                    break;
                case "4":
                    Ejercicio_4();
                    break;
                case "0":
                    return true;

                default:
                    Console.WriteLine("Esta opció no existeix");
                    break;
            }

            return false;
        }


        void Ejercicio_1()
        {
            var metodosDir = new Exercicis();
            string path = EntrarDades.GetString("Escriu el path d'un arxiu o directori.");

            if (metodosDir.DirectorioExist(path))
            {
                Console.WriteLine("Es un directorio");
                metodosDir.ShowRutaAbsoluta(path);
            }
            else if (metodosDir.ArchivoExist(path))
            {
                Console.WriteLine("Es un archivo");
                metodosDir.ShowRutaAbsoluta(path);
            }
            else
                Console.WriteLine("El path ({0}) no existe", path);

            Console.ReadLine();
        }


        void Ejercicio_2()
        {
            var metodosDir = new Exercicis();
            string path;
            string chose;
            do
            {
                path = EntrarDades.GetString("Escribe el path de un directorio.");
                if (metodosDir.HaveExtension(path))
                    Console.WriteLine("¡Has introducido un archivo!");
            } while (metodosDir.HaveExtension(path) || !metodosDir.DirectorioExist(path));

            do
            {
                chose = EntrarDades.GetUpperString("Quieres ver la respuesta como string[S] o como Archivo y directorios[A]?");
            } while (chose != "A" && chose != "S");

            Console.ForegroundColor = ConsoleColor.Green;
            if (chose == "A")
                metodosDir.ListarDirectorioFicheroInfo(path);
            else
                metodosDir.LlistarDirectorioFicheroString(path);
            Console.ResetColor();
            Console.ReadLine();
        }

        void Ejercicio_3()
        {
            var metodosDir = new Exercicis();
            string path;
            string chose;
            do
            {
                path = EntrarDades.GetString("Escribe el path de un directorio.");
                if (metodosDir.HaveExtension(path))
                    Console.WriteLine("¡Has introducido un archivo!");
            } while (metodosDir.HaveExtension(path));

            if (metodosDir.DirectorioExist(path))
            {
                chose = EntrarDades.GetUpperString("El directorio existe: ¿Que quieres hacer, Borrar[B] o cambiar el nombre [C]");
                if (chose == "B")
                {
                    metodosDir.BorrarDirectorio(path);
                }
                else if (chose == "C")
                {
                    string fullPath = Path.GetFullPath(path);
                    metodosDir.ChangeName(fullPath);
                }
            }
            else
            {
                chose = EntrarDades.GetUpperString("El directorio no existe ¿quieres crearlo? Si[S], No[N]");
                if (chose == "S")
                {
                    metodosDir.CrearDirectori(path);
                }
                else
                    Console.WriteLine("No se ha realizado ningun cambio.");
            }
        }

 
        void Ejercicio_4()
        {
            var metodosDir = new Exercicis();
            string path;
            do
            {
                path = EntrarDades.GetString("Escribe el path de un directorio.");
                if (metodosDir.HaveExtension(path))
                    Console.WriteLine("¡Has introducido un archivo!");
            } while (metodosDir.HaveExtension(path));
            metodosDir.ListarDirectorioFicheroInfoRecursive(path);
            Console.ReadKey();
        }
    }
}
